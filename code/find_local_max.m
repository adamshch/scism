function varargout = find_local_max(X, varargin)

% lm_locs = find_local_max(X, x_thresh)
%
% Find local maxima in 2D. This function simply looks at the differentials
% between elements of X and finds locations where the differentials along
% each direction (horizontal, vertical and diagonal) are of opposite signs.
% 
% Inputs:
%    X        - Matrix of entries (real valued)
%    x_thresh - OPTIONAL thresholding to exclude small noise (default is
%               0.01*max(X))
%
% Output:
%    lm_locs   - Locations of local maxima (if one output)
%    lm_i,lm_j - i,j locations of local maxima (if two outputs)
%    
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing

if nargin > 1
    x_thresh = varargin{1};
else
    x_thresh = 1e-2*max(X(:));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Caclulate derivatives and find maxima

[N1,N2,n_t] = size(X);
X(X<x_thresh) = 0;

lm_ind = zeros(N1,N2,n_t);
if n_t > 1
    lm_locs = cell(n_t,1);
end

for kk = 1:n_t
    Xd1 = X(1:(N1-1),:,kk) - X(2:N1,:,kk);                                 % Vertical derivatives
    Xd2 = X(:,1:(N2-1),kk) - X(:,2:N2,kk);                                 % Horizontal derivatives
    Xd3 = X(1:(N1-1),1:(N2-1),kk) - X(2:N1,2:N2,kk);                       % diagonal derivatives 1
    Xd4 = X(1:(N1-1),2:N2,kk) - X(2:N1,1:(N2-1),kk);                       % diagonal derivatives 2

    lm_tmp = (Xd1(1:(end-1),2:(end-1))<0)&(-Xd1(2:end,2:(end-1))<0)&...
        (Xd2(2:(end-1),1:(end-1))<0)&(-Xd2(2:(end-1),2:end)<0)&...
        (Xd3(1:(end-1),1:(end-1))<0)&(-Xd3(2:end,2:end)<0)&...
        (Xd4(1:(end-1),2:end)<0)&(-Xd4(2:end,1:(end-1))<0);                % Find where differentials in and out of a given location are opposite in all directions

    lm_ind(2:(N1-1),2:(N2-1),kk) = lm_tmp;                                 % Make the same size as X
    if n_t > 1
        lm_locs{kk} = find(lm_ind(:,:,kk)==1);                                     % Find all indexed locations
    else
        lm_locs = find(lm_ind==1);                                         % Find all indexed locations
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output Parsing

if nargout > 1
    if n_t > 1
        lm_i = cell(n_t,1);
        lm_j = cell(n_t,1);
        for kk = 1:n_t
            [lm_i{kk},lm_j{kk}] = ind2sub([N1,N2],lm_locs{kk});            % Get subscript vesion of indexed locations
        end
    else
        [lm_i,lm_j] = ind2sub([N1,N2],lm_locs);                            % Get subscript vesion of indexed locations
    end
end

if nargout > 2
    if n_t > 1
        lm_vals = cell(n_t,1);
        for kk = 1:n_t
            X_tmp = X(:,:,kk);
            lm_vals{kk} = X_tmp(lm_locs{kk});                              % Get values at maax locations
        end
    else
        lm_vals = X(lm_locs);                                              % Get values at maax locations
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set outputs based on what is asked for

if nargout == 1
    varargout{1} = lm_locs;
elseif nargout == 1
    varargout{1} = lm_i;
    varargout{2} = lm_j;
elseif nargout > 2
    varargout{1} = lm_i;
    varargout{2} = lm_j;
    varargout{3} = lm_vals;
elseif nargout > 3
    varargout{1} = lm_i;
    varargout{2} = lm_j;
    varargout{3} = lm_vals;
    varargout{4} = lm_ind;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%