function pair_dict = make_pair_shift_dict2d(varargin)

% pair_dict = make_pair_shift_dictionary2d(varargin)
%
% Get dictionary for shifts and distances for gaussians or volcanos pairs
%
% The inputs to this function are
%     d_params:  A struct of parameters, potentially including: 
%         - N:          2D array indicating the spatial size for the
%                       dictionary elements
%         - max_shift:  Maximum shift for shifts
%         - sigma:      2D array for the internal and external Gaussian
%                       bumps 
%         - sep_bound:  2D array with minimum and maximum separation
%                       distances to include
%         - mid_amp:    Depression amount at the center of the volcano
%                       shape
%         - Ndists:     Number of distances to include in the dictionary
%         - pair_type:  Choose 'donut' or 'gauss' shapes
%         - inc_single: Option to include a shape with zero separation
%         - norm_type:  Option to normalize the dictionary elements
% The Output of this function is
%      pair_dict: A 3D array of the dictionary elements (pairs of shapes at
%                 different distances)
% 
% 2016 - Adam Charls

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing

if nargin > 0
    d_params = varargin{1};                                                % Make sure that the first input is a struct
else
    d_params.NOTHING = 0;
end

if (~isfield(d_params,'N'))||isempty(d_params.N)
    N = [100,100];
else
    N = d_params.N;
end

if (~isfield(d_params,'max_shift'))||isempty(d_params.max_shift)
    T = 10;
else
    T = d_params.max_shift;
end

if (~isfield(d_params,'sigma'))||isempty(d_params.sigma)
    a = 2;
else
    a = d_params.sigma;
end

if (~isfield(d_params,'sep_bound'))||isempty(d_params.sep_bound)
    z = [30,60];
else
    z = d_params.sep_bound;
end

if (~isfield(d_params,'mid_amp'))||isempty(d_params.mid_amp)
    b = 0.7;
else
    b = d_params.mid_amp;
end

if (~isfield(d_params,'Ndists'))||isempty(d_params.Ndists)
    Ndists = 10;
else
    Ndists = d_params.Ndists;
end

if (~isfield(d_params,'pair_type'))||isempty(d_params.pair_type)
    pair_type = 'donut';
else
    pair_type = d_params.pair_type;
end

if (~isfield(d_params,'inc_single'))||isempty(d_params.inc_single)
    inc_single = 0;
else
    inc_single = d_params.inc_single;
end

if strcmp(pair_type, 'donut') == 1
    if numel(a) == 1
        a(2) = 0.5*a;
    else
    end
else
end

if nargin > 1
    norm_type = varargin{2};
else
    norm_type = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initializations

z = z(1:2);
T_end(1) = ceil(sqrt(-2*max(abs(a))^2*log(0.01)));
if (T+T_end(1)) > ceil(N(1)/2)
    N(1) = 2*(T+T_end(1));
end
T_end(2) = ceil(sqrt(-2*max(abs(a))^2*log(0.01)));
if (T+T_end(2)+max(z/2)) > ceil(N(2)/2)
    N(2) = 2*(T+T_end(2)+max(z/2));
end
samp_pts1 = (-ceil(N(1)/2):ceil(N(1)/2)).';                                % Get N points between max and min values
samp_pts2 = (-ceil(N(2)/2):ceil(N(2)/2)).';                                % Get N points between max and min values
if T > 0;
    T_pts = linspace(-T,T,ceil(max(N)/15));
else
    T_pts = 0;
end
if inc_single == 1
    z_pts = [0,linspace(min(z),max(z),Ndists-1)];                          % Set up Ndists (include zero distance if asked for)
else
    z_pts = linspace(min(z),max(z),Ndists);                                % Set up Ndists
end

if numel(a) == 1
    a1 = a(1);
elseif numel(a) >= 2
    a1 = max(a);
    a2 = min(a);
end

switch pair_type                                                           % Create anonymouse function for each stereotyped pair
    case 'gauss'
        pair_fun = @(x1,x2,mu,dz) -2*sqrt(2*pi*a1) + exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus,x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus,x2(:).', reshape(mu(2,:) - dz/2,[1,1,size(mu,2)])).^2)/a1) ...
            + exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus,x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus,x2(:).', reshape(mu(2,:) + dz/2,[1,1,size(mu,2)])).^2)/a1);
    case 'donut'
        pair_fun = @(x1,x2,mu,dz) exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus,x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus,x2(:).', reshape(mu(2,:) - dz/2,[1,1,size(mu,2)])).^2)/a1) ...
            + exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus,x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus,x2(:).', reshape(mu(2,:) + dz/2,[1,1,size(mu,2)])).^2)/a1)...
            - b*exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus, x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus, x2(:).', reshape(mu(2,:) - dz/2,[1,1,size(mu,2)])).^2)/a2) ...
            - b*exp(-0.5*bsxfun(@plus, ...
            bsxfun(@minus, x1(:), reshape(mu(1,:),[1,1,size(mu,2)])).^2, ...
            bsxfun(@minus, x2(:).', reshape(mu(2,:) + dz/2,[1,1,size(mu,2)])).^2)/a2)...
            ;
    otherwise
        error('Invalid shape choice.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Make Different Pairs

pair_dict = [];                        
for ll = 1:numel(T_pts)
    mu_x = [T_pts(:).';T_pts(ll)*ones(1,numel(T_pts))];
    for kk = 1:numel(z_pts)
        TMP = reshape(pair_fun(samp_pts1, samp_pts2, mu_x, z_pts(kk)),[],numel(T_pts));
        pair_dict = cat(2,pair_dict,TMP/max(TMP(:)));
    end
end

pair_dict = reshape(pair_dict,[numel(samp_pts1),numel(samp_pts2),Ndists]);

for kk = 1:numel(z_pts)                                                    % This section normalizes the dictionary elements
    if norm_type == 0                                                      % If requested, make each dictionary element have zero-mean
        pair_dict(:,:,kk) = pair_dict(:,:,kk) - mean(mean(pair_dict(:,:,kk)));
    end                                                                    
    pair_dict(:,:,kk) = pair_dict(:,:,kk)...
                          /(norm(reshape(pair_dict(:,:,kk),[],1)));        % Give each dictionary element unit norm (post optional mean subtraction)
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
