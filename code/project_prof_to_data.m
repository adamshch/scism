function prj_img = project_prof_to_data(Xdata,i_max,j_max,temp_img,varargin)

% prj_img = project_prof_to_data(Xdata,i_max,j_max,temp_img,varargin)
% 
% Project a generic ROI to data. This function takse a 3D data array (i.e.
% calcium imaging movie) and a template and returns the projection of the
% data onto that template. The projection calculates 
%
%  sum_t[X_t*<X_t,D>*T_lambda(<X_t,D>/||X_t||_2)] 
%
% This function takes in the inputs
%
%    Xdata       - 3D data array
%    i_max       - scalar first-dimension location of template
%    j_max       - scalar second-dimension location of template
%    temp_img    - template shape
%    SAcuts      - threshold for sparsity in the projection
%    mask_thresh - template threshold for area to average over
% 
% The output is
%    prj_img     - 2D projected image 
%
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parse Inputs

if nargin > 4
    SAcuts = varargin{1};
else
    SAcuts = [0.5,0.85];
end

if nargin > 5
    mask_thresh = varargin{2};
else
    mask_thresh = 0.01;
end

if nargin > 6
    verb = varargin{3};
else
    verb = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Project onto data

prj_img  = zeros(size(Xdata,1),size(Xdata,2),1);                             % Initialize ROI              
prj_img(i_max,j_max) = 1;                                                  % Set ROI location
prj_img  = conv2(prj_img,temp_img,'same');                                 % Make prototypical volcano shape at the ROI location
prj_img  = prj_img/norm(prj_img(:));                                       % Normalize to make some next few lines cleaner
tmp_mask = prj_img>(mask_thresh*max(prj_img(:)));                          % Make a mask for the current ROI
prj_act  = sum(sum(bsxfun(@times,Xdata,prj_img.*tmp_mask),1),2);            % Get temporary time trace activity
prj_act  = prj_act./sqrt(sum(sum(bsxfun(@times,Xdata,tmp_mask).^2,1),2));   % Normalize to get spectral angle
prj_act(isnan(prj_act)) = 0;                                               % Remove Nans at locations with no signal                                        

if verb > 1
    fprintf('Maximum SA is %f, number of frames to avg = %d (or %d)\n',...
        max(prj_act(:)), sum(prj_act>=SAcuts(2)*max(prj_act(:))), ...
        sum((prj_act>SAcuts(1))&(prj_act>=SAcuts(2)*max(prj_act(:)))))     % Output some stats on SA
end
prj_act(prj_act<SAcuts(2)*max(prj_act(:))) = 0;                            % Remove locations with weak matching shape

if size(Xdata,3) == 1
    prj_img = mean(bsxfun(@times,Xdata,tmp_mask*max(prj_img(:))),3);
else
    prj_img = tmp_mask.*mean(bsxfun(@times,Xdata,prj_act),3);               % Choose the best shape based on activity
end
prj_img(isnan(prj_img)) = 0;                                               % Remove potential NaNs
prj_img(prj_img<0)      = 0;                                               % Remove negative values
% prj_img = prj_img - min(vec(prj_img(prj_img>0)));                          % Shift ROI so that the minimum value is zero (smooth to edges)

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%