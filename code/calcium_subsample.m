function mov_sub = calcium_subsample(mov, sub_rates, varargin)

% mov_sub = calcium_subsample(mov, sub_rates)
% 
% Function to subsample image cube. 
%
% The inputs to this function are
%     mov:         MxNxT fluorescence array
%     sub_rates:   3-element array indicating how much to subsample in
%                  space and average in time. First two elements control
%                  sub-sampling in the 1st and 2nd dimension (spatial
%                  dimensions). Last element controls how many frames to
%                  include in the temporal rolling-average.
%     t_ssamp:     OPTIONAL specification to perorm temporal subsampling in
%                  addition to the rolling average
%
% The Outputs of this function are
%      mov_sub:     3D-array of sub-sampled movie.
% 
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing

if nargin > 2
    t_ssamp = varargin{1};
    if isempty(t_ssamp)
        t_ssamp = 0;
    end
else
    t_ssamp = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Subsample image

sum_cube = ones(sub_rates);                                                % Create convolutional summing cube
sum_cube = sum_cube/numel(sum_cube);                                       % Make this an averaging operation
mov      = convn(mov,sum_cube,'same');                                     % Perform convolution
mov_sub  = mov(1:sub_rates(1):end,1:sub_rates(2):end,...
    (sub_rates(3)+1):(end-sub_rates(3)));                                  % Subsample in space/extract time traces

if t_ssamp == 1
    mov_sub = mov_sub (:,:,1:sub_rates(3):end);                            % Maybe subsample in time
else
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%