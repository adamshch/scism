function peak_sel = peak_choose(lm_vals,lm_i,lm_j,h_dist,varargin)

% peak_sel = peak_choose(peak_list,h_dist,varargin)
% 
% Curate a list of peaks to pick multiple peaks at a minimum distance 
% 
% The inputs to this function are
%      lm_locs:   Locations of local maxima (if one output)
%      lm_i,lm_j: i,j locations of local maxima (if two outputs)
%      h_dist:    1- or 2-element array of distances to exclude local peaks
%                 at. The first element controls horizontal closeness and
%                 the second element controls vertical closeness. If a
%                 1-element array, then the second element is automatically
%                 set to Inf.
% 
% % The Outputs of this function are
%      peak_sel:  List of selected peaks. 
% 
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input parsing

if numel(h_dist) == 1
    h_dist = [h_dist, Inf];
end

if iscell(lm_i)
    K = numel(lm_i);
    pk_locs = [];
    pk_list = [];
    for kk = 1:K
        pk_locs = cat(1,pk_locs,[lm_i{kk},lm_j{kk},kk*ones(size(lm_i{kk}))]);
        pk_list = cat(1,pk_list,lm_vals{kk});
    end
else
    pk_locs = [lm_i,lm_j,ones(size(lm_i))];
    pk_list = lm_vals;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run through peaks

num_rem  = numel(pk_list);
% idx_list = 1:numel(pk_list);
peak_sel = [];
while num_rem > 0
    [~,idx_mp] = max(pk_list);
    peak_sel   = cat(1, peak_sel, [pk_locs(idx_mp,:),pk_list(idx_mp)]);    % Move selected paek to the new array
    
    too_close = abs(bsxfun(@minus, pk_locs, pk_locs(idx_mp,:)));
    too_close = [too_close(:,1)< h_dist(1), too_close(:,2)< h_dist(2)];    % Test closeness in each dimension
    too_close = too_close(:,1)&too_close(:,2);                             % Test joint closeness
    pk_list   = pk_list(~too_close);                                       % Remove close-by peaks
    pk_locs   = pk_locs(~too_close,:);                                     
    num_rem   = numel(pk_list);                                            % Count number of remaining peaks
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%