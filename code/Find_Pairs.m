function [NeurProf, NeurTT, found_params] = Find_Pairs(data_path, imgs_to_load, save_base, sub_samp_factor, mov_segment, params_vec, params_vec2, varargin)

% [NeurProf, NeurTT, found_params] = Find_Pairs(data_path, imgs_to_load, save_base, sub_sample_factor, params_vec, params_vec2, varargin)
%
% Function to find pairs of nerons in a movie using matching pursuit
% The inputs to this function are
%     data_path:       String of the path to the directory containing the
%                      image data
%     imgs_to_load:    Cell array containing the strings for the names of
%                      all the files to analyze (in case the data was split
%                      into multile files)
%     save_base:       String to base all the output files on. If empty,
%                      then nothing is saved and all outputs are just
%                      returned
%     sub_samp_factor: 3-element array of how much to sub-sample and
%                      average the data. First two elements tell how much
%                      to sub-sample in space. Last element tells how many
%                      temporal frames to average
%     mov_segment:     Specify if SCISM should only operate on a certain
%                      range of rows (input as 2-element vector of first
%                      and last rows to operate on)
%     params_vec:      Cell array of parameters for specifying the
%                      dictionary shapes to search for
%     params_vec2:     Cell array of parameters for the SCISM algorithm
% 
% The outputs of this function are
%     NeurProf         3D array of found neural profiles
%     NeurTT           2D array of neural profile time-traces
%     found_params
%         - i_max:     Rx1 array of approximate x-axis locations of ROIs
%         - j_max:     Rx1 array of approximate y-axis locations of ROIs
%         - t_est:     Rx1 array of approximate image distances for all 
%                      ROIs (proportional to depth)
%         - d_sel:     Rx1 array indicating which dictionary element was
%                      used to select each ROI
%         - BGprof:    Estimated background image
%         - BGtt:      Time-course for the background frame
% 
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Data

if isempty(data_path)
    data_path = './';
end
if isempty(imgs_to_load)
    imgs_to_load = {'demo_movie.tif'};
end

if isempty(save_base)
    save_base = [];                                                        % Set base for save files
end

if isempty(sub_samp_factor)
    sub_samp_factor = [1,1,5];                                             % Choose subsampling factors
end

if isempty(mov_segment)
    mov_segment = [0,Inf];                                                 % Choose subsampling factors
end

if isempty(params_vec)
    params_vec = {[0.7,4],0.1,[14,25],10};                                 % Choose parameter vector [tau1,tau2,alpha,beta,eta,n_reps]
end

if isempty(params_vec2)
    params_vec2 = {100,0.05,[],[],3000};                                   % Choose second parameter vector [nProfiles, lambda_t, v_thresh, bg_type, batch_sz]
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add pathes

home_path = '/jukebox/pillow/adam/';                                       % Set home path
addpath(genpath([home_path,'calcium_imaging']))                            % Add CI code-base
addpath(genpath('/jukebox/Bezos/OB4_LargeFOVmicroscope/'))                 % Data Locations
addpath(genpath([home_path,'tlsa_matlab']))                                % Factorization Packages
% Optimization Packages
addpath(genpath([home_path,'CodeBaseRepo/External_Code/OptimizationPackages/TFOCS-master/']))
addpath(genpath([home_path,'CodeBaseRepo/External_Code/PlottingCode/ParforProgressBar']))

clear home_path;                                                           % Cleanup


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Data

fprintf('Loading Data...\n')
fprintf('Using data at %s \n', data_path)       
mov = single([]);                                                          % For memory considerations, concatenate to the same array
if strcmp(imgs_to_load{1}(end-3:end),'.tif')
    for kk = 1:numel(imgs_to_load)
        fprintf('Loading file %s ...\n', [data_path,imgs_to_load{kk}])       
        mov = cat(3,mov,single(tiff_reader([data_path,imgs_to_load{kk}],[]))); % Load i'th dataset and append data
    end
elseif strcmp(imgs_to_load{1}(end-3:end),'fits')
    for kk = 1:numel(imgs_to_load)
        fprintf('Loading file %s ...\n', [data_path,imgs_to_load{kk}])       
        mov = cat(3,mov,single(fitsread([data_path,imgs_to_load{kk}])));   % Load i'th dataset and append data
    end
end

fprintf('Data Loaded.\n')       
fprintf('Data validity test: Number of NaNs =  %d.\n', sum(isnan(mov(:)))) % Check Data for NaNs

if sum(isnan(mov(:))) > 0
    fprintf('Found NaN values. Removing NaN rows and columns.\n')
    row_test = ~isnan(sum(mov(:,round(size(mov,2)/2),:),3)');              % Find rows of NaNs
    col_test = ~isnan(sum(mov(round(size(mov,1)/2),:,:),3)');              % Find columns of NaNs
    mov = mov(row_test,col_test,:);                                        % Remove bad rows and columns 
    fprintf('Removed %d rows and %d columns.\n',...
                    sum(row_test==0),sum(col_test==0))
    clear row_test col_test
else 
    fprintf('No NaN values: proceeding with analysis.\n') 
end

fprintf('Data Loaded.\n subsampling video...')       
mov = calcium_subsample(mov, max(sub_samp_factor,1));                      % Average data for higher SNR
mov_size = size(mov);                                                      % Get size of movie
mov = mov/max(mov(:));                                                     % Normalize to the max
fprintf('Subsampling complete, movie is now %d x %d x %d.\n', ...
          size(mov,1),size(mov,2),size(mov,3))                             % Sanity check on movie size 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set up save names

band_lims(1) = max(1,min(mov_segment));                                    % Set up slice limits 
band_lims(2) = min(mov_size(1),max(mov_segment));
if ~isempty(save_base)
    save_path = [data_path,'profiles/'];                                   % Create save path
    if exist(save_path,'dir')~=7                                           % Check that save folder exists
        mkdir(save_path);                                                  % Make the save directory if it does not exist
    end
    save_base = sprintf('%s%s_%d-%d_LR%d',save_path,save_base,...
        band_lims(1),band_lims(2),params_vec2{1});                         % Create base filename for saving results
    save_mat = [save_base,'.mat'];                                         % Create save name for mat-file of results
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Some Parameter Setups

fprintf('Creating dictionary of neural shapes...')
dict_params.N          = [5,60];                                           % Minimum image segment size
dict_params.sigma      = params_vec{1};                                    % Inner/outer gaussian widths
dict_params.mid_amp    = params_vec{2};                                    % Middle amplitude
dict_params.sep_bound  = params_vec{3};                                    % Separation bounds
dict_params.max_shift  = 0;
dict_params.Ndists     = params_vec{4};
dict_params.pair_type  = 'donut';
dict_params.inc_single = 0;
dict_mat = make_pair_shift_dict2d(dict_params);                            % Get Matching Pursuit Dictionary
fprintf('Dictionary created.\n')       

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Perform time-aware OMP

fprintf('Initializing pair finding...\n')       
fprintf('Extrating segment from %d to %d\n',band_lims(1),band_lims(2))
mov = mov(band_lims(1):band_lims(2),:,:);                                  % Option to operate on a segment of the whole movie

% Function Parameters
mp_params.nProf      = params_vec2{1};                                     % Choose number of profiles to find
mp_params.sp_nneg    = params_vec2{2};                                     % Set sparsity parameter for time-traces
mp_params.v_thresh   = params_vec2{3};                                     % Set soft-threshold parameter for temporal inner product averaging
mp_params.bg_type    = params_vec2{4};                                     % Set the choice in background image

% Output/Visualization parameters
mp_params.batch_sz   = params_vec2{5};                                     % Number of frames for batch processing for convolutions and LASSO
mp_params.plot_opt   = 1;                                                  % Ploting flag (1 is to plot, 2 is to save as a video)
mp_params.verbose    = 1;                                                  % Verbose output?
mp_params.mov_comp   = [];
mp_params.sep_bound  = dict_params.sep_bound;                              % Set separation bounds
mp_params.res_vid    = 0;                                                  % Request residual video
if ~isempty(save_base)
    mp_params.vid_name   = [save_base,'.avi'];                             % Set video save name
    mp_params.save_every = 5;                                              % Set intermediate save frequency
    mp_params.save_name  = save_mat;                                       % Set file to save intermediate results to
else
    mp_params.vid_name   = 'Noname.avi';                                   % Set video save name
    mp_params.save_every = 0;                                              % Set intermediate save frequency
    mp_params.save_name  = [];                                             % Set file to save intermediate results to    
end

if nargin > 7                                                              % Choose whether to initialize profiles
    fprintf('Starting with the set of profiles in %s.\n',[save_path,varargin{1}])
    load([save_path,varargin{1}],'rep_mat','NeurProf');                    % Try to load previous profile set from given file
    if exist('rep_mat','var')&&(~exist('NeurProf','var'))                  % Parse what name was used in the old file
        NeurProf = rep_mat;
    elseif exist('NeurProf','var')
        % Do nothing
    else 
        NeurProf = [];                                                     % Initialize with empty profile set
    end
else
    fprintf('Starting with an empty set of profiles.\n')
    NeurProf = [];                                                         % Initialize with empty profile set
end
NeurProf = single(NeurProf);                                               % Make sure matrices are singles

fprintf('Starting pair finding: looking for %d neurons.\n', mp_params.nProf)       
[NeurProf, NeurTT, found_params] = SCISM(mov,dict_mat,mp_params,NeurProf);  % Run Matching pursuit
fprintf('Ended pair finding: saving data.\n')       

fprintf('Saving data to %s.\n', save_mat)       
if ~isempty(save_base)
    save(save_mat,'NeurProf','NeurTT','found_params','dict_mat','mp_params');   % Save data
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
