function y = conv_movfreq2(x, h, varargin)

% y = conv_movfreq2(x, h, conv_size)
% 
% Calculate many 2D convolutions using a frequency-domain calculation
%
% The inputs to this function are
%     x:         MxNxT array of frames to convolve with
%     h:         KxL kernel to convolve with
%     conv_size: OPTIONAL specification of the size of the output (either
%                'same' or 'full'). The function will interpret this option
%                the same way 'conv' does in MATLAB. 
%
% The Outputs of this function are
%      y:        Array of convolved data
%
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing 

if nargin > 2
    conv_size = varargin{1};
else
    conv_size = 'full';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute FFT-based Convolution

L1 = size(x,1) + size(h,1) - 1;                                            % Get size of output in the first dimension
L2 = size(x,2) + size(h,2) - 1;                                            % Get size of output in the second dimension

y = real(ifft2(bsxfun(@times,fft2(x, L1, L2), single(fft2(h, L1, L2)))));  % Calculate output of convolution via FFTs

switch conv_size
    case 'full'
        % do nothing
    case 'same'
        y_ix = ceil((size(h,1)-1)/2) + [1,size(x,1)];
        y_jx = ceil((size(h,2)-1)/2) + [1,size(x,2)];
        y = y(y_ix(1):y_ix(2),y_jx(1):y_jx(2),:);
    otherwise
        % do nothing
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
