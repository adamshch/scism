function [NeurProf, NeurTT, varargout] = SCISMsmall(Fmov,D_vecs,mp_params,varargin)

% [NeurTT, varargout] = SCISMsmall(Fmov,D_vecs,mp_params,varargin)
% 
% Modified MP to demix vTWINS imaging data. This function takes the set of
% dictionary elements in D_vecs and locates similar-looking neuronal ROIs
% in the calcium imaging video Fmov. The dictionary elements found at each
% iteration are then adapted to the data via a correlation-guided data
% projection. An "orthogonalization" operation at each step removes the
% contribution of the set of found profiles at each iteration. This
% operation is not exactly orthogonalization because this operation is
% accomplished via either a non-negative LASSO or a non-negative
% least-squares operation. The main difference between SCISMsmall.m and
% SCISM.m is that SCISMsmall.m reduces the number of convolutions needed to
% run SCISM, at the cost of a much higher memory requirement (thus
% SCISMsmall is only suited for small datasets). 
%
% The inputs to this function are
%     Fmov:      MxNxT calcium imaging video (3D array of doubles or singles -
%                singles are recommended for memory efficiency).  
%     D_vecs:    KxLxP array of dictionary elements that look like the
%                desired neuronal ROIs. For TwINS imaging, this should be a
%                set of images that are annuli of a given width and
%                separated by varying distances
%     mp_params: A struct of parameters, potentially including: 
%         - nProf:       Number of nuronal ROIs to find (default 50)
%         - nBGs:        Number of background templates (default ~T/5000)
%         - proj_type:   Type of projection to penalize (default 'l2')
%         - sp_nneg:     Choice to add a Lasso penalty to (default 0)
%         - v_thresh:    Soft-threshold parameter for time-averaging
%         - plot_opt:    Choose whether to plot on each iteration
%         - bg_type:     Choose if background is 'median' or 'mode'
%         - sep_bound:   max/min separation bounds for neural images
%         - verbose:     Choose to output step-by-step updates
%         - res_vid:     Choose to periodically view residual movies
%         - mov_comp:    
%         - vid_name:    Video output name
%         - save_every:  Save ROIs every 'save_every' iterations
%         - save_name:   Save to this file-name
%      NeurProf: An (optional) initialization for the set of ROIs
%
% The Outputs of this function are
%      NeurProf:     MxNxR array of ROIs found
%      NeurTT:       RxT array of time traces for the found ROIs
%      found_params: Struct containing parameters related to the ROIS:
%         - i_max:     Rx1 array of approximate x-axis locations of ROIs
%         - j_max:     Rx1 array of approximate y-axis locations of ROIs
%         - t_est:     Rx1 array of approximate image distances for all 
%                      ROIs (proportional to depth)
%         - d_sel:     Rx1 array indicating which dictionary element was
%                      used to select each ROI
%         - BGprof:    Estimated background image
%         - BGtt:      Time-course for the background frame
%
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing

if nargin > 3
    NeurProf = varargin{1};                                                 % If provided, initialize ROIs to an initial set
else
    NeurProf = [];                                                          % Otherwise, initialize ROIs to an empty set
end

if (~isfield(mp_params,'nProf'))||isempty(mp_params.nProf)                 % By default look for 50 ROIs
    S = 50;
else
    S = mp_params.nProf;
end

if (~isfield(mp_params,'nBGs'))||isempty(mp_params.nBGs)                   % By default look for max(floor(T/5000),1) ROIs
    nBGs = max(floor(size(Fmov,3)/5000),1);
else
    S = mp_params.nBGs;
end

if (~isfield(mp_params,'sp_nneg'))||isempty(mp_params.sp_nneg)             % By default do not include sparsity in the non-negative least-squares step
    sp_nneg = 0;
else
    sp_nneg = mp_params.sp_nneg;
end

if (~isfield(mp_params,'v_thresh'))||isempty(mp_params.v_thresh)           % Set threshold for pair finding inner product (change to 0.05*max(Fmov(:)) ?)
    mp_params.v_thresh = [];
else
end

if (~isfield(mp_params,'batch_sz'))||isempty(mp_params.batch_sz)           % By default use computational batch size of 3000
    batch_sz = 3000;
else
    batch_sz = mp_params.batch_sz;
end


if (~isfield(mp_params,'proj_type'))||isempty(mp_params.proj_type)         % By default use the sum-of-squared to project data
    proj_type = 'l2';
else
    proj_type = mp_params.proj_type;
end

if (~isfield(mp_params,'bg_type'))||isempty(mp_params.bg_type)             % By default use the median image for the background
    bg_type = 'median';
else
    bg_type = mp_params.bg_type;
end

if (~isfield(mp_params,'sep_bound'))||isempty(mp_params.sep_bound)         % Create default separation bounds (max- and min- distances between pairs). Do not trust absolute distance outputs if this is set incorrectly
    mp_params.sep_bound = [12,80];
else
end

if (~isfield(mp_params,'verbose'))||isempty(mp_params.verbose)             % By default use the lowest level of verbosity
    verb = 0;
else
    verb = mp_params.verbose;
end

if (~isfield(mp_params,'plot_opt'))||isempty(mp_params.plot_opt)           % By default do not plot temporary information
    mp_params.plot_opt = 0;
else
end

if (~isfield(mp_params,'vid_name'))||isempty(mp_params.vid_name)           % Create default movie name
    mp_params.vid_name = '~/Dropbox/CalciumMovieShare/pair_finding_k50_00004c_MP2a.avi';
else
end

if (~isfield(mp_params,'mov_comp'))||isempty(mp_params.mov_comp)           % By default to not 
    mp_params.mov_comp = [];
else
end

if (~isfield(mp_params,'res_vid'))||isempty(mp_params.res_vid)             % By default do not create residual movies
    mp_params.res_vid = 0;
else
end

if (~isfield(mp_params,'save_every'))||isempty(mp_params.save_every)       % By default do not save in the intermediate
    mp_params.save_every = 0;
else
end

if (~isfield(mp_params,'save_name'))||isempty(mp_params.save_name)          % By default save somewhere useless
    mp_params.save_name = './TMP_SAVE.mat';
else
end

if (size(D_vecs,3) == 1)
    if size(D_vecs,1) == size(Fmov,1)*size(Fmov,2)
        D_vecs = reshape(D_vecs,[sqrt(size(D_vecs,1)),sqrt(size(D_vecs,1)),size(D_vecs,2)]);
    end
else
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Basic quantity calculations and Initializations

N1   = size(Fmov,1);                                                       % Get image sizes - space width
N2   = size(Fmov,2);                                                       % Get image sizes - space height
n_t  = size(Fmov,3);                                                       % Get image sizes - number of time steps
n_pc = size(D_vecs,3);                                                     % Get number of shapes to compare

kk        = 0;                                                             % Initialize number of neurons found
NeurTT    = [];                                                            % Initialize empty time-trace array
i_max     = [];                                                            % Initialize i-location values
j_max     = [];                                                            % Initialize j-location values
t_est     = [];                                                            % Initialize depth values
d_sel     = [];
z_pts = linspace(min(mp_params.sep_bound),max(mp_params.sep_bound),...
    size(D_vecs,3)).';                                                     % Set up distances vector

if mp_params.plot_opt > 0 
    n_colors = 8;                                                          % Set number of colors for color-blob image
    mean_img  = (mean(Fmov,3) + 10)./(var(Fmov,[],3)+10);                  % Get an activity related image to display for comparison
    cmap = colormap('hsv');                                                % Get a colormap for depth-color plotting
    cmap = [interp(cmap(:,1),2),interp(cmap(:,2),2),interp(cmap(:,3),2)];  % Interpolate colormap to the number of depths
    cmap(cmap>=1) = 1;cmap(cmap<=0) = 0;                                   % Ensure positive colors
    c_lims = [0,0.8*max(mean_img(:))];                                     % Set image contrast level
    if exist('distinguishable_colors.m','file')==2
        dist_colors = distinguishable_colors(n_colors,{'w','k'});          % Get list of distinguishable colors
    else
        dist_colors = colormap('hsv');                                     % Get list of built-in colors
        dist_colors = dist_colors(round(linspace(1,size(dist_colors,1),...
            n_colors)),:);
    end
    c_to_use = [];                                                         % Initialize color setup
end

if verb > 0
    fprintf('Calculating initialization variables.\n');
    fprintf('    Setting background frames...');
end

if size(Fmov,3) == 1                                                       % If the data is one image then background subtraction is silly
    BGprof = zeros(N1,N2);                                                 
else                                                                       % Calculate the set of background frames for background subtraction
    BGprof = zeros(N1,N2,nBGs);                                            % Initialize background templates
    T_sep = round(linspace(1,n_t,nBGs+1));                                 % Get time separations
    for kk = 1:nBGs
        switch bg_type
            case 'median'
                BGprof(:,:,kk) = median(Fmov(:,:,T_sep(kk):T_sep(kk+1)),3);% Calculate the median frame for median subtraction
            case 'mode'
                BGprof(:,:,kk) = permute(reshape(halfSampleMode(permute(...
                    reshape(Fmov(:,:,T_sep(kk):T_sep(kk+1)),[],N1*N2), ...
                    [3,2,1])), N2, N1),[2,1]);                             % Use the half-sample mode to calculate the mode of the data
            otherwise
                error('Unknown background selction mode!')
        end

        if verb > 0
            fprintf(',%d',kk);
        end
    end
end

if verb > 0
    fprintf('.\n');
end

for mm = 1:nBGs
    if norm(vec(BGprof(:,:,mm))) > eps
        BGprof(:,:,mm) = BGprof(:,:,mm)/norm(vec(BGprof(:,:,mm)));         % Normalize the median frame to its l2 norm
    else
        warning('This video seems to already be median subtracted!')
    end
end

if mp_params.plot_opt > 1
    writerObj = VideoWriter(mp_params.vid_name);                           % Initialize Video
    writerObj.FrameRate = 1;                                               % Set video frame-rate
    open(writerObj)                                                        % Open the video file
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run MP: Initial inner product search

if verb > 0
    fprintf('Initializing inner product arrays.\n')
end
V_array = single(zeros(N1,N2,n_t,n_pc));
parfor ll = 1:n_t                                                          % calculate v = Phi'*y Projection
    for mm = 1:n_pc
        V_array(:,:,ll,mm) = single(conv_freq2(Fmov(:,:,ll), ...
            D_vecs(:,:,mm), 'same'))
    end
end         
V_array = permute(V_array,[3,1,2,4]);

if verb > 0
    fprintf('Initializing median inner products.\n')
end

V_med = zeros(N1,N2,n_pc,nBGs);
for ll = 1:n_pc                                                            % calculate v = Phi'*y Projection for background images
    for mm = 1:nBGs  
        V_med(:,:,ll,mm) = single(conv_freq2(BGprof(:,:,mm), ...
                                              D_vecs(:,:,ll), 'same'));
    end
end

V_ROIs = single([]);
if ~isempty(NeurProf)                                                       % calculate v = Phi'*y Projection for starter ROIs
    N_roi0 = size(NeurProf,3);
    fprintf('Starting with %d ROIS...\n', size(NeurProf,3))
    V_ROIs = single([]);
    for nn = 1:size(NeurProf,3)
        v_tmp = zeros(N1,N2,n_pc);
        for mm = 1:n_pc
            v_tmp(:,:,mm) = single(conv_freq2(NeurProf(:,:,nn), D_vecs(:,:,mm), 'same'));
        end
        V_ROIs = single(cat(4,V_ROIs,v_tmp));
    end
    clear v_tmp
else
    N_roi0 = 0;
end

if isempty(mp_params.v_thresh)||isnan(mp_params.v_thresh)                  % If empty or NaN, Make a default array of threshold values
    % (mean(Fmov,3) + 10)./(var(Fmov,[],3)+10);
    mp_params.v_thresh = 0.05*squeeze(prctile(V_array(:,:,:,1),99,1));     % Calculate threshold based on fraction of 99th percentile
elseif (numel(mp_params.v_thresh) == 1)&&(mp_params.v_thresh<1)&&(mp_params.v_thresh>=0) % If positive and in [0,1], set a global threshold as a fraction of the maximum total projection value
    mp_params.v_thresh = mp_params.v_thresh*max(max(max(max(V_array))));
     if verb > 1
         fprintf('...Using the thresholding parameter %f\n', mp_params.v_thresh)
         fprintf('...Maximum convolution array value is %f\n', max(max(max(max(V_array)))))
     end
elseif (numel(mp_params.v_thresh) == 1)&&(mp_params.v_thresh<0)            % If scalar and negative, set a default global threshold
    mp_params.v_thresh = 0.02*max(max(max(max(V_array))));
     if verb > 1
         fprintf('...Using the thresholding parameter %f\n', mp_params.v_thresh)
         fprintf('...Maximum convolution array value is %f\n', max(max(max(max(V_array)))))
     end
elseif (numel(mp_params.v_thresh) > 1)&&(size(mp_params.v_thresh,1)~=N1)&&(size(mp_params.v_thresh,2)~=N2) % Otherwise, make sure that the threshold is either a scalar or the same size as the video
    error('Need to give as a threshold [], NaN, a scalar, or a matrix the same size as the image!')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run MP: subtract median

if verb > 0
    fprintf('Initializing residual.\n');
end
if isempty(NeurProf)
    BGprof = reshape(BGprof, [], nBGs);                                    % Reshape background shapes into a matrix
    BGtt = (BGprof.'*BGprof)\(BGprof.'*reshape(Fmov, [], n_t));            % Set up initial projection
    Fres = bsxfun(@minus, Fmov, reshape(BGprof*BGtt,size(Fmov)));          % Initialize Residual by subtracting time-varying baseline
    BGprof = reshape(BGprof, [N1, N2, nBGs]);                              % Reshape background shapes back into images
else
    ROI_TMP = reshape(NeurProf, [], size(NeurProf, 3));                    % Reshape current pairs into matrix
    NeurTT  = times_from_profs(Fmov, NeurProf, sp_nneg, BGprof, batch_sz); % Calculate LASSO or NNLS estimates of transients
    BGtt    = NeurTT((end-nBGs+1):end,:);                                  % Extract median (background/baseline) trace
    NeurTT  = NeurTT(1:(end-nBGs),:);                                      % Extract the ROI traces
    Fres    = single(Fmov - reshape(ROI_TMP*NeurTT, size(Fmov))...         
              - reshape(reshape(BGprof,[],nBGs)*BGtt, size(Fmov)));        % Calcualte residual by removing ROI and background contributions
end

v = zeros(N1, N2, n_pc);                                                   % Initialize the heat-map array

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run MP: Iteratively get new ROIs

while (kk<S)&&((size(NeurProf,3)-N_roi0)<S)
    tic;
    kk = kk+1;
    if verb > 0
        fprintf('Calculating Inner Products\n');
    end
    Nd = size(NeurProf,3);                                                 % Get curent number of ROIs
    for ii = 1:N1
    for ll = 1:N2
        v_tmp = V_array(:, ii, ll, :) - sum(bsxfun(@times, ...
            reshape(V_med(ii, ll, :,:), [1 1 1 n_pc nBGs]), ...
            reshape(BGtt.', [n_t 1 1 1 nBGs])),5);                         % Always subtract background
        if ~isempty(NeurProf)
            v_tmp = v_tmp - sum(bsxfun(@times, reshape(V_ROIs(ii, ll, :, :), ...
                [1 1 1 n_pc Nd]), reshape(NeurTT.', [n_t 1 1 1 Nd])),5);   % If any ROIs are found, subtract those contributions too
        else
        end
        if numel(mp_params.v_thresh) > 1                                   % Get the correct threshold if locally defined
            thresh_now = mp_params.v_thresh(ii,ll);
        else                                                               % Otherwise get the global threshold
            thresh_now = mp_params.v_thresh;               
        end
        v_tmp = max(v_tmp - thresh_now, 0);                                % Perform one-sided soft thresholding
        if verb > 1
            fprintf('...Raw num NaNs is %f\n', sum(isnan(v_tmp(:))))
            fprintf('...Raw row max is %f\n', max(v_tmp(:)))
            fprintf('...Thresh row max is %f\n', max(v_tmp(:)))
        end
        if strcmp(proj_type,'l1')    
            v(ii, ll, :) = squeeze(mean(abs(v_tmp), 1));                   % For l1-measures just sum absolute values through time
        elseif strcmp(proj_type,'l2')    
            v(ii, ll, :) = squeeze(mean(v_tmp.^2, 1));                     % For l2-measures (recommended) sum squared values through time
        else
            error('Unknown projection type!') 
        end
    end
    end
        
    %% MP test
    if verb > 0
        fprintf('Selecting ROI(s) at iteration %d\n',kk);
    end
    if verb > 1
        fprintf('Max/min of heat map = [%f,%f]\n',min(v(:)), max(v(:)))
    end
    
    [lm_i,lm_j,lm_vals] = find_local_max(v, 0);                            % Get a list of local maxima
    peak_sel = peak_choose(lm_vals,lm_i,lm_j,7);                           % Refine the list of peaks
    N_nProf = size(peak_sel,1);                                            % Get number of new ROIs
    if isempty(peak_sel)
        fprintf('No ROIs found (no local maxima). Ending algorithm.\n')
        break;
    end
    if verb > 1
        fprintf('......Number of local max to choose from is %d\n',numel(lm_vals))
    end
    
    %% Get new represenational matrix
    if verb > 0
        fprintf('Updating ROI shape:');
    end
    prj_img = single(zeros(N1,N2,N_nProf));
    for mm = 1:N_nProf
        prj_img(:,:,mm) = project_prof_to_data(Fres,peak_sel(mm,1),peak_sel(mm,2),...
             D_vecs(:,:,peak_sel(mm,3)),[0.5,0.75],0.01,verb);             % Project each ROI onto data
    end
    sig_ROIs = sum(sum(prj_img.^2,1),2)> eps;                              % Find significant ROIs
    if verb > 1
        fprintf('Using %d of %d extracted peaks...', numel(sig_ROIs), N_nProf)
    end
    prj_img = prj_img(:,:,sig_ROIs);                                       % Isolate significant ROIs
    i_max   = cat(1,i_max,peak_sel(sig_ROIs,1));                           % Store [i,j] indexing
    j_max   = cat(1,j_max,peak_sel(sig_ROIs,2));                           % Store [i,j] indexing
    d_sel   = cat(1,d_sel,peak_sel(sig_ROIs,3));                           % Store [i,j] indexing
    t_est   = cat(1,t_est,z_pts(peak_sel(sig_ROIs,3)));  
    
    prj_img = bsxfun(@times,prj_img,1./sqrt(sum(sum(prj_img.^2,1),2)));
    if norm(prj_img(:)) > eps
        NeurProf = single(cat(3, NeurProf, prj_img));                      % Re-Normalize ROI and add it to the found ROI set
        if verb > 0
            fprintf('Adding %d ROIs to list.\n',size(prj_img,3))
        end
        for nn = 1:size(prj_img,3)
            v_tmp = zeros(N1,N2,n_pc);
            for mm = 1:n_pc
                v_tmp(:,:,mm) = single(conv_freq2(prj_img(:,:,nn), D_vecs(:,:,mm), 'same'));
            end
            V_ROIs = single(cat(4,V_ROIs,v_tmp));
        end
        clear v_tmp
    else
        fprintf('Found an all-zero ROIs. Assuming no more ROIs to find.\n')
        fprintf('Ending OMP algorithm.\n')
        break                                                              % If all zero ROI - no more ROIs to find
    end
    
    %% Non-negative least squares using TFOCS
    if verb > 0
        fprintf('Orthogonalizing residual: finding time traces...')
    end

    ROI_TMP = reshape(NeurProf, [], size(NeurProf, 3));                    % Reshape current pairs into matrix
    NeurTT  = times_from_profs(Fmov, NeurProf, sp_nneg, BGprof, batch_sz); % Calculate LASSO or NNLS estimates of transients
    BGtt    = NeurTT((end-nBGs+1):end,:);                                  % Extract median (background/baseline) trace
    NeurTT  = NeurTT(1:(end-nBGs),:);                                      % Extract the ROI traces
    Fres    = single(Fmov - reshape(ROI_TMP*NeurTT, size(Fmov))...         
              - reshape(reshape(BGprof,[],nBGs)*BGtt, size(Fmov)));        % Calcualte residual by removing ROI and background contributions
    
    if verb > 0
        fprintf('finished.\n')
    end
    
    %% Ploting Scripts
    if mp_params.plot_opt > 0                                              % Some plotting to keep track of the algorithm
        figure(101);
        sb_sz = [4,4];
        sb_exts = {[1,2,5,6],[3,4,7,8],[9,16]};
        joint_image = zeros(N1*N2, 3);                                     % Initialize joint image
        c_to_use = cat(1,c_to_use,dist_colors(randsample(n_colors,...
            size(prj_img,3),true), :));                                    % Pick colors for new ROIs
        for nn = 1:size(NeurProf,3)                                        % Create joint image 
            rep_tmp = vec(medfilt2(NeurProf(:,:,end-nn+1),[3,3]));         % Iterate backwards so that stronger ROIs appear on top
            joint_image(rep_tmp>0.2*max(rep_tmp),:) = ...
                ones(sum(rep_tmp>0.2*max(rep_tmp)),1)*c_to_use(end-nn+1,:);% Set the color for the ROI pixel locations
        end
        % Plot found ROIs
        subplot(sb_sz(1),sb_sz(2),sb_exts{1}), cla
        subplot(sb_sz(1),sb_sz(2),sb_exts{1}), hold on
        subplot(sb_sz(1),sb_sz(2),sb_exts{1}), imagesc(reshape(joint_image, ...
            [N1, N2, 3]))                                                  % Plot ROI color image
        for nn = 1:(size(NeurProf,3)-N_roi0)
            subplot(sb_sz(1),sb_sz(2),sb_exts{1}), line([j_max(nn)+...
                0.5*t_est(nn);j_max(nn)-0.5*t_est(nn)], [i_max(nn); ...
                i_max(nn)],'Color',cmap(1+mod(nn-1,size(cmap,1)),:))       % Plot lines indicating depth
        end
        axis image
        set(gca,'XTick',[],'YTick',[], 'YDir', 'reverse','XLim',[1,N2],'YLim',[1,N1])
        title('Found ROIs', 'FontSize', 18)
        subplot(sb_sz(1),sb_sz(2),sb_exts{1}), hold off
        % Plot Overlay on data fano facor
        subplot(sb_sz(1),sb_sz(2),sb_exts{2}), cla
        subplot(sb_sz(1),sb_sz(2),sb_exts{2}), hold on
        subplot(sb_sz(1),sb_sz(2),sb_exts{2}), imagesc(mean_img,c_lims)
        for nn = 1:(size(NeurProf,3)-N_roi0)
            subplot(sb_sz(1),sb_sz(2),sb_exts{2}), line([j_max(nn)+...
                0.5*t_est(nn);j_max(nn)-0.5*t_est(nn)], [i_max(nn); ...
                i_max(nn)],'Color',cmap(1+mod(nn-1,size(cmap,1)),:))       % Plot lines indicating depth
            subplot(sb_sz(1),sb_sz(2),sb_exts{2}), scatter([j_max(nn)+...
                0.5*t_est(nn);j_max(nn)-0.5*t_est(nn)], [i_max(nn); ...
                i_max(nn)], 40, 'MarkerEdgeColor',cmap(1+mod(nn-1,...
                size(cmap,1)),:));                                         % Plot circles around ROI locations
        end
        axis image
        axis off
        set(gca,'XTick',[],'YTick',[], 'YDir', 'reverse','XLim',[1,N2],'YLim',[1,N1])
        title('Variance/Mean of Data', 'FontSize', 18)
        colormap gray
        subplot(sb_sz(1),sb_sz(2),sb_exts{2}), hold off
        % Plot time traces of new ROIs
        traces_to_plot = 0.9*(NeurTT((end-size(prj_img,3)+1):end,:).')/...
            max(vec(NeurTT((end-size(prj_img,3)+1):end,:))) + ...
            ones(size(NeurTT((end-size(prj_img,3)+1):end,:),2),1)*...
            (1:size(NeurTT((end-size(prj_img,3)+1):end,:),1));             % Create array of new time traces
        subplot(sb_sz(1),sb_sz(2),sb_exts{3}), plot((1:size(traces_to_plot,1))/30, traces_to_plot,'-b')
        title('New Time Traces', 'FontSize', 18)
        xlabel('Time (s)', 'FontSize', 18)
        ylabel('Neuron Number', 'FontSize', 18)
        set(gca, 'XLim', [1,size(traces_to_plot,1)]/30, 'YLim', [0,size(traces_to_plot,2)+1]);
        set(gcf,'color',[1,1,1]);
        drawnow
        if mp_params.plot_opt > 1
            writeVideo(writerObj,getframe(gcf));
        end
    else                                                                   % No plotting
        if verb>0
            fprintf('Plotting Suppressed')
        end
    end
    
    if mod(kk,mp_params.save_every) == 0
        found_params.i_max     = i_max;                                    % Output i-locations of ROIs 
        found_params.j_max     = j_max;                                    % Output j-locations of ROIs
        found_params.t_est     = t_est;                                    % Output depth-locations of ROIs (which shape) 
        found_params.d_sel     = d_sel;                                    % Output depth-locations of ROIs (distance)
        found_params.BGprof    = BGprof;                                   % Output median frame
        found_params.BGtt      = BGtt;                                     % Output median frame's time-course
        save(mp_params.save_name,'NeurProf','NeurTT','found_params',...
            'mp_params','D_vecs','-v7.3')                                  % Save big data efficiently with version 7.3 
    end
    
    ITER_TIME = toc;                                                       % Get time for k'th iteration
    if verb>0
        fprintf('Iteration %d Done. Iteration time: %5.5f seconds. Number of neurons found: %d\n',kk,ITER_TIME,size(NeurProf,3))
    end
end
if mp_params.plot_opt > 1
    close(writerObj);                                                      % Close video
end

fprintf('Finished finding ROIs, performing one last time-trace estimation.\n')
if ~isempty(NeurProf)
    NeurTT  = times_from_profs(Fmov, NeurProf, sp_nneg, BGprof, batch_sz); % Do one final update to the time traces
    BGtt    = NeurTT((end-nBGs+1):end,:);                                  % Extract median (background/baseline) trace
    NeurTT  = NeurTT(1:(end-nBGs),:);                                      % Extract the ROI traces
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set Outputs

if nargout > 2
    varargout{1}.i_max     = i_max;                                        % Output i-locations of ROIs 
    varargout{1}.j_max     = j_max;                                        % Output j-locations of ROIs
    varargout{1}.t_est     = t_est;                                        % Output depth-locations of ROIs (which shape) 
    varargout{1}.d_sel     = d_sel;                                        % Output depth-locations of ROIs (distance)
    varargout{1}.BGprof    = BGprof;                                       % Output median frame
    varargout{1}.BGtt      = BGtt;                                         % Output median frame's time-course
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
