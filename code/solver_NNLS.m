function x = solver_NNLS( A, b, x0, opts )

% SOLVER_L1RLS l1-regularized least squares problem, sometimes called the LASSO.
% [ x, odata, opts ] = solver_NNLS( A, b, x0, opts )
%    Solves the non-negative least squares problem
%        minimize (1/2)*norm( A * x - b )^2  s.t. x > 0
%    using the Auslender/Teboulle variant with restart. A must be a matrix
%    or a linear operator, b must be a vector. The initial point x0 and
%    option structure opts are both optional.
%
% The inputs to this function are
%     A:         Linear operator in A(x) = b (see TFOCS documentation for
%                specifics instructions on specifying A)
%     b:         Target vector in A(x) = b
%     x0:        Initial guess for the solution
%     opts:      Struct of options for TFOCS (see TFOCS documentation)
%
% The Outputs of this function are
%      x:        Solution of NNLS
% 
% 2016 - Adam Charles (modified from solver_L1RLS in the TFOCS package)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set up problem for TFOCS

narginchk(3,5);
if nargin < 4, x0 = []; end
if nargin < 5, opts = []; end
if ~isfield( opts, 'restart' ), 
    opts.restart = 100; 
end

nonneg = false;
if isfield(opts,'nonneg')
    nonneg  = opts.nonneg;
    opts = rmfield(opts,'nonneg');
end
if isfield(opts,'nonNeg')
    nonneg  = opts.nonNeg;
    opts = rmfield(opts,'nonNeg');
end
prox    = proj_Rplus;                                                      % Set positive constraint

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve Non-negative least-squares
[x,~,~] = tfocs( smooth_quad, { A, -b }, prox, x0, opts );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lisence Info for original solver_L1RLS function of which this is an
% adaptation of: 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TFOCS v1.3 by Stephen Becker, Emmanuel Candes, and Michael Grant.
% Copyright 2013 California Institute of Technology and CVX Research.
% See the file LICENSE for full license information.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%