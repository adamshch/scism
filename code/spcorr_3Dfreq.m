function v = spcorr_3Dfreq(x, h, v_thresh,varargin)

% v = spcorr_3Dfreq(x, h, v_thresh,varargin)
% 
% Calculate a function of the 2D convolutions between a 3D array (movie
% object) and a set of kernels using frequency-domain calculations. 
%
% The inputs to this function are
%     x:          MxNxT array of frames to convolve with
%     h:          KxLxP set of kernels to convolve with
%     v_thresh:   Threshold for restricted correlation calculations
%     proj_type:  OPTIONAL specification of what type of summation to
%                 perform ('l1' for the sum of correlations and 'l2' for
%                 the sum of square correlations) - default is 'l2'
%     batch_size: OPTIONAL specification of batch size to operate on (helps
%                 with memory constraints. Default is 3000
%
% The Outputs of this function are
%      v:         MxNxP array of calculated values
%     
%
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input Parsing 

if nargin > 3
    proj_type = varargin{1};
    if isempty(proj_type)
        proj_type = 'l2';
    end
else
    proj_type = 'l2';
end

if nargin > 4
    batch_size = varargin{2};
else
    batch_size = 3000;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute FFT-based Convolution

L1 = size(x,1) + size(h,1) - 1;                                            % Get size of output in the first dimension
L2 = size(x,2) + size(h,2) - 1;                                            % Get size of output in the second dimension

y_ix = ceil((size(h,1)-1)/2) + [1,size(x,1)];
y_jx = ceil((size(h,2)-1)/2) + [1,size(x,2)];

N_gaps = ceil(size(x,3)/batch_size);                                       % Calculate number of batches
T_gaps = round(linspace(0,size(x,3),N_gaps+1));                            % Get batch limits

v = zeros(size(x,1), size(x,2), size(h,3),'single');

for kk = 1:N_gaps
    mov_freq = single(fft2(single(x(:,:,(T_gaps(kk)+1):T_gaps(kk+1))), ...
                                                                 L1, L2));% Take FFT of matrix
    for mm = 1:size(h,3)
        v_tmp = real(ifft2(bsxfun(@times,mov_freq, ...
                                   single(fft2(h(:,:,mm), L1, L2)))));     % Calculate output of convolution via FFTs
        v_tmp = v_tmp(y_ix(1):y_ix(2),y_jx(1):y_jx(2),:);

        if numel(v_thresh) == size(x,1)*size(x,2)                          % Get the correct threshold if spatially defined
            v_tmp = max(bsxfun(@minus, v_tmp, v_thresh),0);                % Perform one-sided soft thresholding
        elseif numel(v_thresh) == size(x,1)*size(x,2)*size(h,3)            % Get the correct threshold if locally defined
            v_tmp = max(bsxfun(@minus, v_tmp, v_thresh(:,:,mm)), 0);       % Perform one-sided soft thresholding
        elseif numel(v_thresh) == 1                                        % Otherwise get the global threshold
            v_tmp = max(v_tmp - v_thresh, 0);                              % Perform one-sided soft thresholding
        else
            error('Bad number of threshold parameters!')
        end

        if strcmp(proj_type,'l1')    
            v(:,:,mm) = v(:,:,mm) + sum(v_tmp,3);                          % Iteratively calculate the ENERGY of each cadidate shape
        elseif strcmp(proj_type,'l2')    
            v(:,:,mm) = v(:,:,mm) + sum(v_tmp.^2,3);                       % Iteratively calculate the ENERGY of each cadidate shape 
        else
        error('Unknown projection type!') 
        end
    end
end

v = v/size(x,3);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
