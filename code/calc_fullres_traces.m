function [NeurTTfull,BGTTfull] = calc_fullres_traces(data_path, imgs_to_load, sub_samp_factor, NeurProf, found_params, varargin)

% [NeurTTfull,BGTTfull] = calc_fullres_traces(data_path, imgs_to_load, sub_samp_factor, NeurProf, found_params, varargin)
%
% Function to find pairs of nerons in a movie using matching pursuit
% The inputs to this function are
%     data_path:       String of the path to the directory containing the
%                      image data
%     imgs_to_load:    Cell array containing the strings for the names of
%                      all the files to analyze (in case the data was split
%                      into multile files)
%     sub_samp_factor: 3-element array of how much to sub-sample and
%                      average the data. First two elements tell how much
%                      to sub-sample in space. Last element tells how many
%                      temporal frames to average
%     NeurProf:        3D array of found neural profiles
%     found_params:    Parameter output from SCISM
%     lambda:          Specify the spatsity trade-off parameter for the
%                      non-nagative LASSO
%     batch_size:      OPTIONAL specification of batch size for computing
%                      the non-negative LASSO
% The outputs of this function are
%     NeurTTfull:      2D array of full resolution neural profile
%                      time-traces
%     BGTTfull:        2D array of full resolution background time-traces
% 
% 2016 - Adam Charles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Data

if isempty(data_path)
    data_path = './';
end
if isempty(imgs_to_load)
    imgs_to_load = {'demo_movie.tif'};
end

if nargin > 5
    lambda = varargin{1};
else
    lambda = 0.04;
end

if nargin > 6
    batch_size = varargin{2};
else
    batch_size = 3000;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add pathes

home_path = '/jukebox/pillow/adam/';                                       % Set home path
addpath(genpath([home_path,'calcium_imaging']))                            % Add CI code-base
addpath(genpath('/jukebox/Bezos/OB4_LargeFOVmicroscope/'))                 % Data Locations
addpath(genpath([home_path,'tlsa_matlab']))                                % Factorization Packages
% Optimization Packages
addpath(genpath([home_path,'CodeBaseRepo/External_Code/OptimizationPackages/TFOCS-master/']))
addpath(genpath([home_path,'CodeBaseRepo/External_Code/PlottingCode/ParforProgressBar']))

clear home_path;                                                           % Cleanup


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Data

fprintf('Loading Data...\n')
fprintf('Using data at %s \n', data_path)       
mov = single([]);                                                          % For memory considerations, concatenate to the same array
if strcmp(imgs_to_load{1}(end-3:end),'.tif')
    for kk = 1:numel(imgs_to_load)
        fprintf('Loading file %s ...\n', [data_path,imgs_to_load{kk}])       
        mov = cat(3,mov,single(tiff_reader([data_path,imgs_to_load{kk}],[]))); % Load i'th dataset and append data
    end
elseif strcmp(imgs_to_load{1}(end-3:end),'fits')
    for kk = 1:numel(imgs_to_load)
        fprintf('Loading file %s ...\n', [data_path,imgs_to_load{kk}])       
        mov = cat(3,mov,single(fitsread([data_path,imgs_to_load{kk}])));   % Load i'th dataset and append data
    end
end

fprintf('Data Loaded.\n')       
fprintf('Data validity test: Number of NaNs =  %d.\n', sum(isnan(mov(:)))) % Check Data for NaNs

if sum(isnan(mov(:))) > 0
    fprintf('Found NaN values. Removing NaN rows and columns.\n')
    row_test = ~isnan(sum(mov(:,round(size(mov,2)/2),:),3)');              % Find rows of NaNs
    col_test = ~isnan(sum(mov(round(size(mov,1)/2),:,:),3)');              % Find columns of NaNs
    mov = mov(row_test,col_test,:);                                        % Remove bad rows and columns 
    fprintf('Removed %d rows and %d columns.\n',...
                    sum(row_test==0),sum(col_test==0))
    clear row_test col_test
else 
    fprintf('No NaN values: proceeding with analysis.\n') 
end

fprintf('Data Loaded...')       
mov = mov/max(mov(:));                                                     % Normalize to the max
mov = calcium_subsample(mov, [sub_samp_factor(1:2),1]);                    % downsample spatially but keep FULL RESOLUTION IN TIME

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculate Time Traces

% N_gaps = ceil(size(mov,3)/batch_size);
% T_gaps = round(linspace(1,size(mov,3),N_gaps+1));

% for kk = 1:N_gaps
%    [NeurTTfull(:,T_gaps(kk):T_gaps(kk+1)), BGTTfull(:,T_gaps(kk):T_gaps(kk+1))] ...
%        = times_from_profs(mov(:,:,T_gaps(kk):T_gaps(kk+1)), NeurProf, ...
%        lambda, found_params.BGprof);                                        % Do one final update to the time traces
%     fprintf('Block %d done.\n',kk)
% end

[NeurTTfull, BGTTfull] = times_from_profs(mov, NeurProf, lambda, ...
     found_params.BGprof,batch_size);                                      % Do one final update to the time traces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
