\documentclass[11pt,lettersize]{article}

\usepackage{amsmath,amssymb,amsthm,bm}
\usepackage{fullpage}
\usepackage{verbatim}


\begin{document}

\title{SCISM Code Package for vTwINS Analysis}

\author{}
\date{}

\maketitle

\tableofcontents

\newpage

\section{Copyright and Disclaimer}

Copyright 2016, Princeton University. All rights reserved. By using this software the USER indicates that he or she has read, understood and will comply with the following:
\newline
\newline
--- Princeton University hereby grants USER nonexclusive permission to use, copy and/or modify this software for internal, noncommercial, research purposes only. Any distribution, including commercial sale or license, of this software, copies of the software, its associated documentation and/or modifications of either is strictly prohibited without the prior consent of Princeton University. Title to copyright to this software and its associated documentation shall at all times remain with Princeton University. Appropriate copyright notice shall be placed on all software copies, and a complete copy of this notice shall be included in all copies of the associated documentation. No right is granted to use in advertising, publicity or otherwise any trademark, service mark, or the name of Princeton University. 
\newline
\newline
--- This software and any associated documentation is provided ``as is'' 
\newline
\newline
PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL PROPERTY RIGHTS OF A THIRD PARTY. 
\newline
\newline
Princeton University shall not be liable under any circumstances for any direct, indirect, special, incidental, or consequential damages with respect to any claim by USER or any third party on account of or arising from the use, or inability to use, this software or its associated documentation, even if Princeton University has been advised of the possibility of those damages.

\newpage
\section{Setup and Requirements}

\subsection{System Requirements}

SCISM is designed to extract neural spatial profiles and time traces from high-dimensional two-photon imaging data. As such, the requirements on a system are primarily memory-based. To run SCISM, the system must be able to hold the twice the video size (as a `single' data type) in memory (once for the video and once for the residual). In addition, many of the intermediate steps are performed in batches, so the requirement that another 2-8GB is required (the batch size can be modulated to better fit the constraints of a given system). 

\subsection{Required Additional Packages}

SCISM is written in MATLAB, so MATLAB is required to use this package. 
As the SCISM algorithm requires large-scale optimization (specifically non-negative LASSO and non-negative least-squares). To perform this optimization, SCISM uses the TFOCS (Templates for First-Order Conic Solvers) software~\cite{becker2011tfocs}. This code package is available freely online (see below for URL). Unfortunately, one function in the TFOCS package causes an error in the SCISM package, so to use TFOCS, the \texttt{prox\_l1pos.m} function in TFOCS needs to be replaced with the version of the function included with the SCISM software. 

The code included with SCISM performs the required averaging necessary in terms of spatial and temporal averaging. The motion correction code, however, is not included, as it is readily available online. To run new data through the SCISM algorithm, motion correction should be run prior to using the code contained in this package. This code is also readily available online (see below for URL).

One additional function that can be used with SCISM is an optional function for plotting neural profiles, and does not change the computation. This function is the `distinguishable colors' package, available on the Mathworks website. This function allows for SCISM to select easy-to-detect colors for profile plotting when a visualization output while the algorithm is running is requested. SCISM will automatically detect if this function is available, and if not, SCISM will default to the MATLAB built-in `hsv' colormap. 

All additional packages used here are available freely online at the following locations:
\begin{itemize}
\item TFOCS: 
\begin{verbatim}
https://github.com/cvxr/TFOCS\end{verbatim}
\item Motion correction: 
\begin{verbatim}
https://github.com/sakoay/princeton-ecs 
https://github.com/sakoay/opencv-ecs\end{verbatim}
\item Distinguishable colors 
\begin{verbatim}
https://www.mathworks.com/matlabcentral/fileexchange/29702-generate-maximally-
perceptually-distinct-colors/content/distinguishable_colors.m\end{verbatim}
\end{itemize}

\subsection{Setup of SCISM}

To set up SCISM, first ensure that the TFOCS package is downloaded and added to MATLAB's directory. If TFOCS is located in the directory \texttt{tfocs\_dir}, then run
\begin{verbatim}
>> addpath(genpath('tfocs_dir'))
\end{verbatim}
to add the TFOCS directory to the path. Next download and unzip the SCISM package and add the directory where SCISM was extracted to (\texttt{scism\_dir}) as
\begin{verbatim}
>> addpath('scism_dir')
\end{verbatim}
To have TFOCS work well with SCISM, the file \texttt{prox\_l1pos} in the SCISM sub-directory \\ TFOCS\_l1pos\_replacement needs to be movied to the TFOCS main directory (to replace the function of the same name already in there). In Unix-based systems, this can be done in the MATLAB command prompt (or a standard command prompt by excluding the exclamation mark) as
\begin{verbatim}
>>! cp scism_dir/TFOCS_l1pos_replacement/prox_l1pos.m tfocs_dir/
\end{verbatim}
and in windows systems this can be done through the windows file-system. 


\newpage
\section{Included Example}

As an example dataset, we include data from early visual cortex of an awake mouse (a segment of the full dataset used in the main vTwINS paper). 
This dataset (demoSpatialBin.tif), was already subjected to the motion correction algorithm described in the required additional packages section, and has already undergone the 2x spatial down-sampling. The template used for motion correction was the median of 10 frame averages of the movie, iterated until the maximum shift between subsequent iterations is less than 0.3 pixels. The dataset was then binned 2x spatially to minimize filesize.

This SCISM algorithm can be run on this data set by specifying the directory the file is in (data\_dir) and a file name to save the results into (save\_name) and using the following command:
\begin{verbatim}
>> [NeurProf, NeurTT, found_params] = Find_Pairs(data_dir,{'demoSpatialBin.tif'}, ...
   save_name,[1,1,5],[0,Inf], {[0.7,4],0.1,[10,43],15},{150,0.001,[],[],3000});
\end{verbatim}
This command will load the data, perform temporal averaging and run the SCISM algorithm on the data in demoSpatialBin.tif. The function will return the spatial profiles (\texttt{NeurProf}), the time traces of the averaged data (\texttt{NeurTT}) and a set of meta-parameters of the neural profiles (\texttt{found\_params}; see function documentation for details). To obtain the full-resolution time traces, the following command can be run on the output of \texttt{Find\_Pairs}
\begin{verbatim}
>> [NeurTTfull,BGTTfull] = calc_fullres_traces(data_dir,{'demoSpatialBin.tif'}, ...
   [1,1,1], NeurProf, found_params, 0.15)
\end{verbatim} 
%\begin{verbatim}
%>> Fmov = tiff_reader('demoSpatialBin.tif',[]);
%>> Fmov = Fmov/max(Fmov(:));
%>> [x_est,x_bg] = times_from_profs(Fmov,Neur_Pro,0.01);
%\end{verbatim} 
This function will return the full time traces for the profiles in \texttt{NeurProf} (\texttt{NeurTTfull}), as well as the time traces associated with background activity (\texttt{BGTTfull}). The final input into \texttt{calc\_fullres\_traces} sets the sparsity parameter for the final time trace estimate, and can be adjusted to increase or decrease the sparsity threshold.
Finally, the 3D locations of each neuron can be obtained from the neural profile by calling 
\begin{verbatim}
>> params = profile2params(NeurProf,z_tilt)
\end{verbatim}
This function uses the neural profiles that SCISM returns, along with a parameter indicating the the beam angle \texttt{z\_tilt}, and returns the 3D locations for each profile. \texttt{z\_tilt} can be calculated from the angle of each beam to the axial direction $\theta$ by \texttt{z\_tilt}~=~$2\mbox{tan}(\theta)*(\#\mbox{pixels}/\mu\mbox{m})$. For this example, $\#\mbox{pixels}/\mu\mbox{m} = 512pix/550\mu\mbox{m}$ for the full-spatial resolution and $\#\mbox{pixels}/\mu\mbox{m} = 256pix/550\mu\mbox{m}$ for the spatially downsampled data, given \texttt{z\_tilt}=0.395.
\newline
\newline
Another example dataset (demoNoBin.tif) includes a small slice of the same dataset at the original resolution. This can be run with a 2x spatial binning as above with the following command:
\begin{verbatim}
>> [NeurProf, NeurTT, found_params] = Find_Pairs(data_dir,{'demoNoBin.tif'},...
   save_name,[2,2,5],[0,Inf], {[0.7,4],0.1,[10,43],15},{150,0.001,[],[],3000});
\end{verbatim}
or with no spatial binning with the following command:
\begin{verbatim}
>> [NeurProf, NeurTT, found_params] = Find_Pairs(data_dir,{'demoNoBin.tif'},...
   save_name,[1,1,5],[0,Inf], {[1.4,8],0.1,[20,86],15},{150,0.001,[],[],3000});
\end{verbatim}
Similarly, this is followed up with an update to the time traces using \texttt{calc\_fullres\_traces} and calculation of the 3D locations using \texttt{profile2params} as
\begin{verbatim}
>> [NeurTTfull,BGTTfull] = calc_fullres_traces(data_dir,{'demoNoBin.tif'}, ...
   [2,2,1], NeurProf, found_params, 0.15)
>> params = profile2params(NeurProf,0.395)
\end{verbatim} 
if the neural profiles were found using spatial binning or as 
\begin{verbatim}
>> [NeurTTfull,BGTTfull] = calc_fullres_traces(data_dir,{'demoNoBin.tif'}, [1,1,1], ...
   NeurProf, found_params, 0.15)
>> params = profile2params(NeurProf,0.79)
\end{verbatim} 
if the neural profiles were found without spatial binning. 


\begin{comment}
If there is no spatial scaling:
\begin{verbatim}
>> Fmov = tiff_reader('demoNoBin.tif',[]);
\end{verbatim}
Or if there is spatial scaling:
\begin{verbatim}
>> Fmov = imresize(tiff_reader('demoNoBin.tif',[]),0.5);
\end{verbatim}
Followed by normalization and reestimation of time traces:
\begin{verbatim}
>> Fmov = Fmov/max(Fmov(:));
>> [x_est,x_bg] = times_from_profs(Fmov,Neur_Pro,0.01);
\end{verbatim}
For both datasets, the estimated 3D position of the cell can be estimated with profiles2params:
\begin{verbatim}
>> params = profiles2params(Neur_pro,0.363)
\end{verbatim}
If there was no spatial binning by a factor of two, the z\_tilt parameter is doubled:
\begin{verbatim}
>> params = profiles2params(Neur_pro,0.725)
\end{verbatim}
\end{comment}

\newpage
\section{Included Functions}

\begin{center}
\begin{table}[ht!] \label{tab:FunList} \caption{List of Functions}
\begin{tabular}{ | l | p{11cm}|} \hline
Function Name & Summary \\ \hline\hline
\texttt{Find\_Pairs.m} & This is the main function that loads a dataset, performs necessary pre-processing (save for motion correction) and runs the SCISM algorithm. This function requires both pointers to the data in terms of directory and file names, as well as the algorithmic parameters to use with the SCISM algorithm and a name to for the filename to save the results to. \\ \hline
\texttt{SCISM.m} & This function is the main SCISM algorithm. It takes in the pre-processed data and a set of algorithm parameters and returns a list of neural profiles, the corresponding time-traces, and various meta-data associated with the neural profiles. \\ \hline
\texttt{SCISMsmall.m} & This function essentially performs the same operation as the SCISM algorithm, however using a computational re-organization to allow for far less required convolutional operations at the cost of additional memory requirements. \\ \hline
\texttt{calc\_fullres\_traces.m} & Function to calculate the full-resolution traces from the found neural profiles. \\ \hline
\texttt{calcium\_subsample.m} & This function performs temporal averaging and spatial downsampling of the data. \\ \hline
\texttt{conv\_movfreq2.m} & This function performs a 2D convolution of each slice of a 3D array (i.e. frames of a video) with a set of kernels using a frequency-domain calculation. \\ \hline
\texttt{find\_local\_max.m} & This function finds a list of local maxima in an image. \\ \hline
\texttt{make\_pair\_shift\_dict2d.m} & This function creates a dictionary of 2D shapes to look for across the images field-of-view. \\ \hline
\texttt{peak\_choose.m} & Function to curate a list of local peaks (i.e the output of \texttt{find\_local\_max.m} to contain peaks only a certain distance away from each other. \\ \hline
\texttt{profile2params.m} & Function to calculate the estimated 3D position of the neuron \\ \hline
\texttt{project\_prof\_to\_data.m} & This function takes a found spatial profile and uses a correlation-based projection to adapt the spatial profile to the fluorescence data. \\ \hline
\texttt{solver\_NNLS.m} & This function is a wrapper for TFOCS that sets up the solver for non-negative least-squares. \\ \hline
\texttt{spcorr\_3Dfreq.m} & This function calculates the sparity-based correlation between a video sequence and a series of kernels. \\ \hline
\texttt{tiff\_reader.m} & Function to read in all or part of a TIFF file. \\ \hline
\texttt{times\_from\_profs.m} & Function which takes in a fluorescence video (3D array) and a set of neural and background profiles, and calculates the time traces using either non-negative least-squares or non-negative LASSO. \\ \hline
\end{tabular}
\end{table}
\end{center}

\subsection*{\texttt{>> Find\_Pairs.m}}

\begin{verbatim}
% [NeurProf, NeurTT, found_params] = Find_Pairs(data_path, imgs_to_load, ...
%              save_base,sub_sample_factor, params_vec, params_vec2, varargin)
%
% Function to find pairs of nerons in a movie using matching pursuit
% The inputs to this function are
%     data_path:       String of the path to the directory containing the
%                      image data
%     imgs_to_load:    Cell array containing the strings for the names of
%                      all the files to analyze (in case the data was split
%                      into multile files)
%     save_base:       String to base all the output files on. If empty,
%                      then nothing is saved and all outputs are just
%                      returned
%     sub_samp_factor: 3-element array of how much to sub-sample and
%                      average the data. First two elements tell how much
%                      to sub-sample in space. Last element tells how many
%                      temporal frames to average
%     mov_segment:     Specify if SCISM should only operate on a certain
%                      range of rows (input as 2-element vector of first
%                      and last rows to operate on)
%     params_vec:      Cell array of parameters for specifying the
%                      dictionary shapes to search for
%     params_vec2:     Cell array of parameters for the SCISM algorithm
% 
% The outputs of this function are
%     NeurProf         3D array of found neural profiles
%     NeurTT           2D array of neural profile time-traces
%     found_params
%         - i_max:     Rx1 array of approximate x-axis locations of ROIs
%         - j_max:     Rx1 array of approximate y-axis locations of ROIs
%         - t_est:     Rx1 array of approximate image distances for all 
%                      ROIs (proportional to depth)
%         - d_sel:     Rx1 array indicating which dictionary element was
%                      used to select each ROI
%         - BGprof:    Estimated background image
%         - BGtt:      Time-course for the background frame
\end{verbatim}
\newpage
\subsection*{\texttt{>> SCISM.m}} 

\begin{verbatim}
% [NeurProf, NeurTT, found_params] = SCISM(Fmov,D_vecs,mp_params,varargin)
% 
% Modified MP to demix vTWINS imaging data. This function takes the set of
% dictionary elements in D_vecs and locates similar-looking neuronal ROIs
% in the calcium imaging video Fmov. This search is performed with a series
% of thresholded correlations, calculated by an FFT-based convolution
% calculation (using the function spcorr 3Dfreq.m). The dictionary elements
% found at each iteration are then adapted to the data via a
% correlation-guided data projection. This data projection averages
% (calculated using project prof to data.m) takes a weighted average of the
% frames most highly correlated to the ideal dictionary element chosen. 
% An "orthogonalization" operation at each step removes the contribution of
% the set of found profiles at each iteration. This operation is not
% exactly orthogonalization, as this operation is accomplished via either a
% non-negative LASSO or a non-negative least-squares operation. These
% operations are perfromed in batches using the TFOCS software, called from
% the function times from profs.m. These steps are iterated until a given
% number of profiles are found, or all time traces of newly found profiles
% are zero: indicating that none of the variance remaining in the movie can
% be explaied using additional profiles. 
% 
%
% The inputs to this function are
%     Fmov:      MxNxT calcium imaging video (3D array of doubles or singles -
%                singles are recommended for memory efficiency).  
%     D_vecs:    KxLxP array of dictionary elements that look like the
%                desired neuronal ROIs. For TwINS imaging, this should be a
%                set of images that are annuli of a given width and
%                separated by varying distances
%     mp_params: A struct of parameters, potentially including: 
%         - nProf:       Number of nuronal ROIs to find (default 50)
%         - nBGs:        Number of background templates (default ~T/5000)
%         - proj_type:   Type of projection to penalize (default 'l2')
%         - sp_nneg:     Choice to add a Lasso penalty to (default 0)
%         - v_thresh:    Soft-threshold parameter for time-averaging
%         - plot_opt:    Choose whether to plot on each iteration
%         - batch_sz:    Batch size for sub-computations
%         - bg_type:     Choose if background is 'median' or 'mode'
%         - sep_bound:   max/min separation bounds for neural images
%         - verbose:     Choose to output step-by-step updates
%         - res_vid:     Choose to periodically view residual movies
%         - mov_comp:    
%         - vid_name:    Video output name
%         - save_every:  Save ROIs every 'save_every' iterations
%         - save_name:   Save to this file-name
%      NeurProf:  An (optional) initialization for the set of ROIs
%
% The Outputs of this function are
%      NeurProf:     MxNxR array of ROIs found
%      NeurTT:       RxT array of time traces for the found ROIs
%      found_params: Struct containing parameters related to the ROIS:
%         - i_max:     Rx1 array of approximate x-axis locations of ROIs
%         - j_max:     Rx1 array of approximate y-axis locations of ROIs
%         - t_est:     Rx1 array of approximate image distances for all 
%                      ROIs (proportional to depth)
%         - d_sel:     Rx1 array indicating which dictionary element was
%                      used to select each ROI
%         - BGprof:    Estimated background image
%         - BGtt:      Time-course for the background frame
\end{verbatim}
\newpage
\subsection*{\texttt{>> SCISMsmall.m}}
\begin{verbatim}
% [NeurProf, NeurTT, found_params] = SCISMsmall(Fmov,D_vecs,mp_params,varargin)
% 
% Modified MP to demix vTWINS imaging data. This function takes the set of
% dictionary elements in D_vecs and locates similar-looking neuronal ROIs
% in the calcium imaging video Fmov. This search is performed with a series
% of thresholded correlations, calculated by an FFT-based convolution
% calculation (using the function spcorr 3Dfreq.m). The dictionary elements
% found at each iteration are then adapted to the data via a
% correlation-guided data projection. This data projection averages
% (calculated using project prof to data.m) takes a weighted average of the
% frames most highly correlated to the ideal dictionary element chosen. 
% An "orthogonalization" operation at each step removes the contribution of
% the set of found profiles at each iteration. This operation is not
% exactly orthogonalization, as this operation is accomplished via either a
% non-negative LASSO or a non-negative least-squares operation. These
% operations are perfromed in batches using the TFOCS software, called from
% the function times from profs.m. These steps are iterated until a given
% number of profiles are found, or all time traces of newly found profiles
% are zero: indicating that none of the variance remaining in the movie can
% be explaied using additional profiles. The main difference between SCISMsmall.m and
% SCISM.m is that SCISMsmall.m reduces the number of convolutions needed to
% run SCISM, at the cost of a much higher memory requirement (thus
% SCISMsmall is only suited for small datasets). 
%
% The inputs to this function are
% Fmov:      MxNxT calcium imaging video (3D array of doubles or singles -
%            singles are recommended for memory efficiency). 
% D_vecs:    KxLxP array of dictionary elements that look like the
%            desired neuronal ROIs. For TwINS imaging, this should be a
%            set of images that are annuli of a given width and
%            separated by varying distances
% mp_params: A struct of parameters, potentially including: 
% - nProf:      Number of nuronal ROIs to find (default 50)
% - nBGs:       Number of background templates (default ~T/5000)
% - proj_type:  Type of projection to penalize (default 'l2')
% - sp_nneg:    Choice to add a Lasso penalty to (default 0)
% - v_thresh:   Soft-threshold parameter for time-averaging
% - plot_opt:   Choose whether to plot on each iteration
% - bg_type:    Choose if background is 'median' or 'mode'
% - sep_bound:  max/min separation bounds for neural images
% - verbose:    Choose to output step-by-step updates
% - res_vid:    Choose to periodically view residual movies
% - mov_comp: 
% - vid_name:   Video output name
% - save_every: Save ROIs every 'save_every' iterations
% - save_name:  Save to this file-name
% NeurProf:  An (optional) initialization for the set of ROIs
%
% The Outputs of this function are
% NeurProf: MxNxR array of ROIs found
% NeurTT: RxT array of time traces for the found ROIs
% found_params: Struct containing parameters related to the ROIS:
% - i_max:      Rx1 array of approximate x-axis locations of ROIs
% - j_max:      Rx1 array of approximate y-axis locations of ROIs
% - t_est:      Rx1 array of approximate image distances for all 
%               ROIs (proportional to depth)
% - d_sel:      Rx1 array indicating which dictionary element was
%               used to select each ROI
% - BGprof:     Estimated background image
% - BGtt:       Time-course for the background frame
\end{verbatim}
\newpage
\subsection*{\texttt{>> calc\_fullres\_traces.m}}

\begin{verbatim}
% [NeurTTfull,BGTTfull] = calc_fullres_traces(data_path, imgs_to_load, ...
%                              sub_samp_factor, NeurProf, found_params, varargin)
%
% Function to find pairs of nerons in a movie using matching pursuit
% The inputs to this function are
%     data_path:       String of the path to the directory containing the
%                      image data
%     imgs_to_load:    Cell array containing the strings for the names of
%                      all the files to analyze (in case the data was split
%                      into multile files)
%     sub_samp_factor: 3-element array of how much to sub-sample and
%                      average the data. First two elements tell how much
%                      to sub-sample in space. Last element tells how many
%                      temporal frames to average
%     NeurProf:        3D array of found neural profiles
%     found_params:    Parameter output from SCISM
%     lambda:          Specify the spatsity trade-off parameter for the
%                      non-nagative LASSO
%     batch_size:      OPTIONAL specification of batch size for computing
%                      the non-negative LASSO
% The outputs of this function are
%     NeurTTfull:      2D array of full resolution neural profile
%                      time-traces
%     BGTTfull:        2D array of full resolution background time-traces
\end{verbatim}
\newpage
\subsection*{\texttt{>> calcium\_subsample.m}} 

\begin{verbatim}
% mov_sub = calcium_subsample(mov, sub_rates)
% 
% Function to subsample image cube. 
%
% The inputs to this function are
% mov: MxNxT fluorescence array
% sub_rates: 3-element array indicating how much to subsample in
% space and average in time. First two elements control
% sub-sampling in the 1st and 2nd dimension (spatial
% dimensions). Last element controls how many frames to
% include in the temporal rolling-average.
% t_ssamp: OPTIONAL specification to perorm temporal subsampling in
% addition to the rolling average
%
% The Outputs of this function are
% mov_sub: 3D-array of sub-sampled movie.
\end{verbatim}
\newpage
\subsection*{\texttt{>> conv\_movfreq2.m}} 

\begin{verbatim}
% mov_sub = calcium_subsample(mov, sub_rates)
% 
% Function to subsample image cube. 
%
% The inputs to this function are
%     mov:         MxNxT fluorescence array
%     sub_rates:   3-element array indicating how much to subsample in
%                  space and average in time. First two elements control
%                  sub-sampling in the 1st and 2nd dimension (spatial
%                  dimensions). Last element controls how many frames to
%                  include in the temporal rolling-average.
%     t_ssamp:     OPTIONAL specification to perorm temporal subsampling in
%                  addition to the rolling average
%
% The Outputs of this function are
%      mov_sub:     3D-array of sub-sampled movie.
\end{verbatim}
\newpage
\subsection*{\texttt{>> find\_local\_max.m}} 

\begin{verbatim}
% lm_locs = find_local_max(X, x_thresh)
%
% Find local maxima in 2D. This function simply looks at the differentials
% between elements of X and finds locations where the differentials along
% each direction (horizontal, vertical and diagonal) are of opposite signs.
% 
% Inputs:
%    X        - Matrix of entries (real valued)
%    x_thresh - OPTIONAL thresholding to exclude small noise (default is
%               0.01*max(X))
%
% Output:
%    lm_locs   - Locations of local maxima (if one output)
%    lm_i,lm_j - i,j locations of local maxima (if two outputs)
\end{verbatim}
\newpage
\subsection*{\texttt{>> make\_pair\_shift\_dict2d.m}} 

\begin{verbatim}
% pair_dict = make_pair_shift_dictionary2d(varargin)
%
% Get dictionary for shifts and distances for gaussians or volcanos pairs
%
% The inputs to this function are
%     d_params:  A struct of parameters, potentially including: 
%         - N:          2D array indicating the spatial size for the
%                       dictionary elements
%         - max_shift:  Maximum shift for shifts
%         - sigma:      2D array for the internal and external Gaussian
%                       bumps 
%         - sep_bound:  2D array with minimum and maximum separation
%                       distances to include
%         - mid_amp:    Depression amount at the center of the volcano
%                       shape
%         - Ndists:     Number of distances to include in the dictionary
%         - pair_type:  Choose 'donut' or 'gauss' shapes
%         - inc_single: Option to include a shape with zero separation
%         - norm_type:  Option to normalize the dictionary elements
% The Output of this function is
%      pair_dict: A 3D array of the dictionary elements (pairs of shapes at
%                 different distances)
\end{verbatim}
\newpage
\subsection*{\texttt{>> peak\_choose.m}}

\begin{verbatim}
% peak_sel = peak_choose(peak_list,h_dist,varargin)
% 
% Curate a list of peaks to pick multiple peaks at a minimum distance 
% 
% The inputs to this function are
%      lm_locs:   Locations of local maxima (if one output)
%      lm_i,lm_j: i,j locations of local maxima (if two outputs)
%      h_dist:    1- or 2-element array of distances to exclude local peaks
%                 at. The first element controls horizontal closeness and
%                 the second element controls vertical closeness. If a
%                 1-element array, then the second element is automatically
%                 set to Inf.
% 
% % The Outputs of this function are
%      peak_sel:  List of selected peaks. 
\end{verbatim}
\newpage
\subsection*{\texttt{>> profile2params.m}}
\begin{verbatim}
% [A_params,paramsM] = profile2params(A,z_tilt)
%
% Function to extract the position parameters from spatial profiles. The
% inputs to this function are
%   A:        The 3D matrix of spatial profiles
%   z-tilt:   An optional input indicating the conversion ratio of px/um 
%             for the vTwINS PSF, calculated as 2*tand(theta), where theta 
%             is the angle of one half of the PSF to normal
% The outputs of this function are
%   A_params: A 4 column vector corresponding to X-position (px),
%             Y-position (px), pair separation (px), and depth (um),
%             estimated using the centroid of each half of the spatial
%             profile.
%   paramsM:  Same as above except the center pixel position of each half
%             of the spatial profile is used instead.
\end{verbatim}
\newpage

\subsection*{\texttt{>> project\_prof\_to\_data.m}}
\begin{verbatim}
% prj_img = project_prof_to_data(Xdata,i_max,j_max,temp_img,varargin)
% 
% Project a generic ROI to data. This function takse a 3D data array (i.e.
% calcium imaging movie) and a template and returns the projection of the
% data onto that template. The projection calculates 
%
%  sum_t[X_t*<X_t,D>*T_lambda(<X_t,D>/||X_t||_2)] 
%
% This function takes in the inputs
%
%    Xdata       - 3D data array
%    i_max       - scalar first-dimension location of template
%    j_max       - scalar second-dimension location of template
%    temp_img    - template shape
%    SAcuts      - threshold for sparsity in the projection
%    mask_thresh - template threshold for area to average over
% 
% The output is
%    prj_img     - 2D projected image 
\end{verbatim}
\newpage
\subsection*{\texttt{>> solver\_NNLS.m}} 

\begin{verbatim}
% SOLVER_L1RLS l1-regularized least squares problem, sometimes called the LASSO.
% [ x, odata, opts ] = solver_NNLS( A, b, x0, opts )
%    Solves the non-negative least squares problem
%        minimize (1/2)*norm( A * x - b )^2  s.t. x > 0
%    using the Auslender/Teboulle variant with restart. A must be a matrix
%    or a linear operator, b must be a vector. The initial point x0 and
%    option structure opts are both optional.
%
% The inputs to this function are
%     A:         Linear operator in A(x) = b (see TFOCS documentation for
%                specifics instructions on specifying A)
%     b:         Target vector in A(x) = b
%     x0:        Initial guess for the solution
%     opts:      Struct of options for TFOCS (see TFOCS documentation)
%
% The Outputs of this function are
%      x:        Solution of NNLS
\end{verbatim}
\newpage
\subsection*{\texttt{>> spcorr\_3Dfreq.m}}

\begin{verbatim}
% v = spcorr_3Dfreq(x, h, v_thresh,varargin)
% 
% Calculate a function of the 2D convolutions between a 3D array (movie
% object) and a set of kernels using frequency-domain calculations. 
%
% The inputs to this function are
%     x:          MxNxT array of frames to convolve with
%     h:          KxLxP set of kernels to convolve with
%     v_thresh:   Threshold for restricted correlation calculations
%     proj_type:  OPTIONAL specification of what type of summation to
%                 perform ('l1' for the sum of correlations and 'l2' for
%                 the sum of square correlations) - default is 'l2'
%     batch_size: OPTIONAL specification of batch size to operate on (helps
%                 with memory constraints. Default is 3000
%
% The Outputs of this function are
%      v:         MxNxP array of calculated values
\end{verbatim}
\newpage
\subsection*{\texttt{>> tiff\_reader.m}}
\begin{verbatim}
% Y = tiff_reader(name,T)
%
% read tiff stack. Optional truncate to first T timesteps 
%
% The inputs to this function are
%     name:      String pointing to the tif file to load
%     T:         Number of frames to extract
% 
% The output to this function is
%     Y:         3D array of the tif frames
\end{verbatim}
\newpage
\subsection*{\texttt{>> times\_from\_profs.m}}
\begin{verbatim}
% [x_est, x_bg] = times_from_profs(mov, NeurProf, lambda, {BGprofs, batch_size, bin_mask})
%
% Find time traces from ROIs and a movie. This function uses the TFOCS
% library to find ROIs and time traces using either non-negative least
% squares, or non-negative LASSO. Specifically, the program solves 
%
%      argmin_{S,S_b>=0} ||Y-X*S-X_b*S_b||_F^2  + sum_{i,j}lambda_{i,j}*|S_{i,j}|
%
% which reduces to NNLS for lambda = 0. lambda can be set as a scalar
% (lambda_{i,j} = lambda is a constant), as a vector 
% (lambda_{i,j} = lambda_i, i.e. each row of S gets a unique lambda), or as
% a matrix specifying all the weights infividually. S_b is NOT regularized
% as it represents the backgroud activity as is expected to be active to
% some degree at all times. 
%
% The inputs to this function are
%     mov:        Fluorescence movie
%     NeurProf:   3D array of neural profiles to find time-traces for
%     lambda:     Scalar, vector or matrix of sparsity values indicating
%                 confidence in activity as transients (Default is 0).
%     BGprofs:    OPTIONAL set of background profiles (Default is median of
%                 input movie mov).
%     batch_size: OPTIONAL batch size for computation (default is 3000).
%     tfocs_Tol:  OPTIONAL TFOCS toleranve (default based on # variables).
%     bin_mask:   OPTIONAL binary mask to discount erroneous pixels
%                 (Default is no mask). 
% 
% The outputs to this function are
%     x_est:      Time traces for neural profiles (same order as stack of
%                 profiles.
%     x_bg:       OPTIONAL time traces for background components. If only
%                 one output is specifird then the last n_BGs number of
%                 rows of x_est will be x_bg
\end{verbatim}




\newpage
\bibliographystyle{plain}
\bibliography{CalciumBib}

\end{document}


